from sqlalchemy import create_engine
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA

import pandas as pd
import mysql.connector
import matplotlib.pyplot as mpl
import time
import requests
import pymysql
import json


#   FOR TRANSFERRING THE LOCAL DB TO AWS
def transferDatabaseToAWS():
    conLOCAL = mysql.connector.connect(user='root', password='masterPassword',
                                       host='127.0.0.1', database='imdbdataset')
    engineAWS = create_engine(
        'mysql://masterUsername:masterPassword@imdbdataset.cylq1mw66qwl.ca-central-1.rds.amazonaws.com:3306/imdbdataset?charset=utf8')
    conAWS = engineAWS.connect()

    imdb_movie = pd.read_sql_query("select * from imdb_movie;", conLOCAL)
    imdb_name = pd.read_sql_query("select * from imdb_name_clean;", conLOCAL)

    imdb_movie.to_sql(name='imdb_movie', con=conAWS, if_exists='replace')
    imdb_name.to_sql(name='imdb_name_clean', con=conAWS, if_exists='replace')


#   FOR TRANSFERRING THE AWS DB TO LOCAL SQL (not tested)
def transferDatabaseToLOCAL():
    conLOCAL = mysql.connector.connect(user='root', password='masterPassword',
                                       host='127.0.0.1', database='imdbdataset')
    engineAWS = create_engine(
        'mysql://masterUsername:masterPassword@imdbdataset.cylq1mw66qwl.ca-central-1.rds.amazonaws.com:3306/imdbdataset?charset=utf8')
    conAWS = engineAWS.connect()

    imdb_movie = pd.read_sql_query("select * from imdb_movie;", conAWS)
    imdb_name = pd.read_sql_query("select * from imdb_name_clean;", conAWS)

    imdb_movie.to_sql(name='imdb_movie', con=conLOCAL, if_exists='replace')
    imdb_name.to_sql(name='imdb_name_clean', con=conLOCAL, if_exists='replace')


#   FOR GETTING MOVIE KEY WORDS FROM TMDB TO BE EVENTUALLY PUT ONTO IMDB DATASET
def getKeyWords(imdb_id):
    response = requests.get(
        f'https://api.themoviedb.org/3/find/{imdb_id}?api_key='
        f'd1f8f0884339145ce8810c0198a9513c&language=en-US&external_source=imdb_id')
    tmdb_id = response.json()['movie_results'][0]['id']
    response = requests.get(
        f'https://api.themoviedb.org/3/movie/{tmdb_id}/keywords?api_key=d1f8f0884339145ce8810c0198a9513c')
    keywords = ''
    for word in response.json()['keywords']:
        keywords += word['name'] + ', '
    return (keywords)


#   FOR UPDATING THE AWS DATASET WITH KEY WORDS INSTEAD OF DESCRIPTIONS
def updateDatabase(imdb_movie, con):

    for x, row in imdb_movie.iterrows():
        imdb_movie['imdb_description'][x] = getKeyWords(imdb_movie['imdb_id'][x])

    imdb_movie.to_sql('imdb_movie_new', con=con, if_exists='replace', index=False)


#   COUNT UNIQUE KEYS COUNTS THE UNIQUE MOVIE TAGS
def countUniqueKeys(imdb_movie):
    print('starting to count unique words')
    start_time = time.time()
    imdb_movie.imdb_description = imdb_movie.imdb_description.str.replace('[^a-zA-Z, ]', '')
    imdb_movie.imdb_description = imdb_movie.imdb_description.str.lower()
    unique_key = imdb_movie.imdb_description.str.get_dummies(sep=", ").sum().sort_values(ascending=False)
    unique_key.to_csv('unique_key.csv')
    print("--- %s seconds ---" % (time.time() - start_time))


#   COUNT UNIQUE CASE COUNTS THE UNIQUE CAST MEMBERS
def countUniqueCast(imdb_movie):
    print('starting to count unique cast')
    start_time = time.time()
    imdb_movie.imdb_cast = imdb_movie.imdb_cast.str.replace('[^a-zA-Z., ]', '')
    imdb_movie.imdb_cast = imdb_movie.imdb_cast.str.lower()
    unique_cast = imdb_movie.imdb_cast.str.get_dummies(sep=", ").sum().sort_values(ascending=False)
    unique_cast.to_csv('unique_cast.csv')
    print("--- %s seconds ---" % (time.time() - start_time))


#   CREATE KEY MAP CREATES A 2D FITTED DATASET FROM A MATRIX OF KEYS
def createKeyMap(imdb_movie):
    start_time = time.time()
    unique_key = imdb_movie.imdb_description.str.split('  ', 10, expand=True)
    unique_key.columns = ['key1', 'key2', 'key3', 'key4', 'key5', 'key6', 'key7', 'key8', 'key9', 'key10', 'key11']
    # The 6th column is the remainder and not used

    unique_key1 = unique_key['key1'].str.get_dummies(sep='  ')
    unique_key2 = unique_key['key2'].str.get_dummies(sep='  ')
    unique_key3 = unique_key['key3'].str.get_dummies(sep='  ')
    unique_key4 = unique_key['key4'].str.get_dummies(sep='  ')
    unique_key5 = unique_key['key5'].str.get_dummies(sep='  ')

    unique_key6 = unique_key['key6'].str.get_dummies(sep='  ')
    unique_key7 = unique_key['key7'].str.get_dummies(sep='  ')
    unique_key8 = unique_key['key8'].str.get_dummies(sep='  ')

    unique_key9 = unique_key['key7'].str.get_dummies(sep='  ')
    unique_key10 = unique_key['key8'].str.get_dummies(sep='  ')

    unique_key = unique_key1.add(unique_key2, fill_value=0)
    unique_key = unique_key.add(unique_key3, fill_value=0)
    unique_key = unique_key.add(unique_key4, fill_value=0)
    unique_key = unique_key.add(unique_key5, fill_value=0)

    unique_key = unique_key.add(unique_key6, fill_value=0)
    unique_key = unique_key.add(unique_key7, fill_value=0)
    unique_key = unique_key.add(unique_key8, fill_value=0)
    unique_key = unique_key.add(unique_key9, fill_value=0)
    unique_key = unique_key.add(unique_key10, fill_value=0)

    unique_key.to_csv('unique_key_matrix_10.csv')

    print("--- %s seconds ---" % (time.time() - start_time))
    # unique_key_map = PCA(n_components=2).fit_transform(unique_key)
    # unique_key_map_df = pd.DataFrame(unique_key_map, columns=['pca1', 'pca2'])
    # unique_key_map_df.to_csv('unique_key_map_ext.csv')


#   CREATE CAST MAP CREATES A 2D FITTED DATASET FROM A MATRIX OF CAST
def createCastMap(imdb_movie):
    start_time = time.time()
    unique_cast = imdb_movie.imdb_cast.str.split('  ', 12, expand=True)
    unique_cast.columns = ['cast1', 'cast2', 'cast3', 'cast4', 'cast5', 'cast6',
                           'cast7', 'cast8', 'cast9', 'cast10', 'cast11', 'cast12', 'cast13']
    # The last column is the remainder and not used

    # Can I shorten this? Yes. Am I lazy? Yes.
    unique_cast1 = unique_cast['cast1'].str.get_dummies(sep='  ')
    unique_cast2 = unique_cast['cast2'].str.get_dummies(sep='  ')
    unique_cast3 = unique_cast['cast3'].str.get_dummies(sep='  ')
    unique_cast4 = unique_cast['cast4'].str.get_dummies(sep='  ')
    unique_cast5 = unique_cast['cast5'].str.get_dummies(sep='  ')

    # unique_cast6 = unique_cast['cast6'].str.get_dummies(sep='  ')
    # unique_cast7 = unique_cast['cast7'].str.get_dummies(sep='  ')
    # unique_cast8 = unique_cast['cast8'].str.get_dummies(sep='  ')
    # unique_cast9 = unique_cast['cast9'].str.get_dummies(sep='  ')
    # unique_cast10 = unique_cast['cast10'].str.get_dummies(sep='  ')
    # unique_cast11 = unique_cast['cast11'].str.get_dummies(sep='  ')
    # unique_cast12 = unique_cast['cast12'].str.get_dummies(sep='  ')

    unique_cast1 = unique_cast1.multiply(2.618)
    unique_cast2 = unique_cast2.multiply(1.618)
    unique_cast3 = unique_cast3.multiply(1.000)
    unique_cast4 = unique_cast3.multiply(0.618)
    unique_cast5 = unique_cast3.multiply(0.382)

    unique_cast = unique_cast1.add(unique_cast2, fill_value=0)
    unique_cast = unique_cast.add(unique_cast3, fill_value=0)
    unique_cast = unique_cast.add(unique_cast4, fill_value=0)
    unique_cast = unique_cast.add(unique_cast5, fill_value=0)
    # unique_cast = unique_cast.add(unique_cast6, fill_value=0)
    # unique_cast = unique_cast.add(unique_cast7, fill_value=0)
    # unique_cast = unique_cast.add(unique_cast8, fill_value=0)
    # unique_cast = unique_cast.add(unique_cast9, fill_value=0)
    # unique_cast = unique_cast.add(unique_cast10, fill_value=0)
    # unique_cast = unique_cast.add(unique_cast11, fill_value=0)
    # unique_cast = unique_cast.add(unique_cast12, fill_value=0)

    print("--- %s seconds ---" % (time.time() - start_time))

    unique_cast.to_csv('unique_cast_matrix_5_mod.csv')
    unique_cast = pd.read_csv('unique_cast_matrix_5_mod.csv', index_col=0)
    unique_cast_fitted = PCA(n_components=2).fit_transform(unique_cast)
    unique_cast_fitted_df = pd.DataFrame(unique_cast_fitted, columns=['pca1', 'pca2'])
    unique_cast_fitted_df.to_csv('unique_cast_fitted_5_mod.csv')


#   CREATE GENRE MAP CREATES A 2D FITTED DATASET FROM A MATRIX OF GENRES
def createGenreMap(imdb_movie):
    imdb_movie.imdb_genre = imdb_movie.imdb_genre.str.replace('[^a-zA-Z ]', '')
    unique_genres = imdb_movie.imdb_genre.str.split(' ', expand=True)
    unique_genres.columns = ['genre1', 'genre2', 'genre3']

    unique_genres1 = unique_genres['genre1'].str.get_dummies(sep=' ')
    unique_genres2 = unique_genres['genre2'].str.get_dummies(sep=' ')
    unique_genres3 = unique_genres['genre3'].str.get_dummies(sep=' ')

    unique_genres1 = unique_genres1.multiply(1.000)
    unique_genres2 = unique_genres2.multiply(0.618)
    unique_genres3 = unique_genres3.multiply(0.382)

    unique_genres = unique_genres1.add(unique_genres2, fill_value=0)
    unique_genres = unique_genres.add(unique_genres3, fill_value=0)

    unique_genres.to_csv('unique_genre_matrix_mod.csv')
    main_genre_1 = PCA(n_components=2).fit_transform(unique_genres)
    main_genre_1_df = pd.DataFrame(main_genre_1, columns=['pca1', 'pca2'])
    main_genre_1_df.to_csv('unique_genre_map_mod.csv')



def generateRecommendationProfiles(imdb_movie, input_title, genreMap, castMap, keyMap):
    genreMap.columns = ['pca1', 'pca2']
    # castMap.columns = ['pca1', 'pca2']
    # keyMap.columns = ['pca1', 'pca2']

    input_movie_genre = genreMap[imdb_movie['imdb_title'] == input_title]
    genreMap['pca1'] = genreMap['pca1'].subtract(input_movie_genre.iloc[:, genreMap.columns.
                                                     get_loc('pca1')].values[0], fill_value=0)
    genreMap['pca2'] = genreMap['pca2'].subtract(input_movie_genre.iloc[:, genreMap.columns.
                                                 get_loc('pca2')].values[0], fill_value=0)
    genreMap['sum'] = genreMap.sum(axis=1, skipna=True)
    genreMap['dist'] = genreMap['sum'].pow(1. / 2)

    # input_movie_key = keyMap[imdb_movie['imdb_title'] == input_title]
    # keyMap['pca1'] = keyMap['pca1'].subtract(input_movie_key.iloc[:, keyMap.columns.
    #                                              get_loc('pca1')].values[0], fill_value=0)
    # keyMap['pca2'] = keyMap['pca2'].subtract(input_movie_key.iloc[:, keyMap.columns.
    #                                          get_loc('pca2')].values[0], fill_value=0)
    # keyMap['sum'] = keyMap.sum(axis=1, skipna=True)
    # keyMap['dist'] = keyMap['sum'].pow(1. / 2)

    result1 = imdb_movie.loc[genreMap.nsmallest(100, 'dist').index]
    result2 = imdb_movie.loc[castMap.nsmallest(100, ['priority', 'dist']).index]
    result3 = imdb_movie.loc[keyMap.nsmallest(100, ['priority', 'dist']).index]
    # result1 = result1['imdb_title'].tolist()
    # result2 = result2['imdb_title'].tolist()
    # result3 = result3['imdb_title'].tolist()
    # result2 = castMap.loc[castMap.nsmallest(100, ['priority', 'dist']).index]
    # result3 = keyMap.loc[keyMap.nsmallest(100, ['priority', 'dist']).index]

    overall = pd.merge(result2, result3)

    # overall = keyMap['dist'] + castMap['dist']
    # overall.columns = ['dist', 'priority']
    # overall['priority'] = keyMap['priority'] + castMap['priority']
    # print(overall)
    # resulto = imdb_movie.loc[overall.nsmallest(100).index]

    with pd.option_context('display.max_columns', None, 'display.max_rows', None):
        # print(result1)
        # print(result2['dist'])
        # print(result3['dist'])
        print(overall)



    # movieMap = genreMap
    # movieMap['pca3'] = keyMap['pca1']
    # movieMap['pca4'] = keyMap['pca2']
    # movieMap['pca5'] = castMap['pca1']
    # movieMap['pca6'] = castMap['pca2']



    # for column, contents in movieMap.items():
    #     movieMap[column] = movieMap[column].subtract(input_movie_coord.iloc[:, movieMap.columns.
    #                                                  get_loc(column)].values[0], fill_value=0)
    #     movieMap[column] = movieMap[column].pow(2)

    # movieMap['pca5'] = castMap.pow(2)

    # movieMap['pca5'] = castMap['pca1']
    # movieMap['pca6'] = castMap['pca2']
    # movieMap['pcadiffsumt'] = movieMap.sum(axis=1, skipna=True)
    # movieMap['pcadiffsum1'] = movieMap['pca1'] + movieMap['pca2']
    # movieMap['pcadiffsum2'] = movieMap['pca3'] + movieMap['pca4']
    # movieMap['pcadiffsum3'] = movieMap['pca5'] + movieMap['pca6']
    # movieMap['pcadiffdist1'] = movieMap['pcadiffsum1'].pow(1. / 2)
    # movieMap['pcadiffdist2'] = movieMap['pcadiffsum2'].pow(1. / 2)
    # movieMap['pcadiffdist3'] = movieMap['pcadiffsum3'].pow(1. / 2)
    # movieMap['pcadiffdistt'] = movieMap['pcadiffsumt'].pow(1. / 2)
    # result1 = imdb_movie.loc[movieMap.nsmallest(30, 'pcadiffdist1').index]
    # result2 = imdb_movie.loc[movieMap.nsmallest(30, 'pcadiffdist2').index]
    # result3 = imdb_movie.loc[movieMap.nsmallest(30, 'pcadiffdist3').index]
    # resultt = imdb_movie.loc[movieMap.nsmallest(30, 'pcadiffdistt').index]
    # result1 = result1['imdb_title'].tolist()
    # result2 = result2['imdb_title'].tolist()
    # result3 = result3['imdb_title'].tolist()
    # resultt = resultt['imdb_title'].tolist()

    # result = pd.DataFrame(result1)
    # result['top key'] = result2
    # result['top cast'] = result3
    # result['top overall'] = resultt
    # df1_styler = result1.style.set_table_attributes("style='display:inline'").set_caption('top genre')
    # df2_styler = result2.style.set_table_attributes("style='display:inline'").set_caption('top cast')
    # df3_styler = result3.style.set_table_attributes("style='display:inline'").set_caption('top keys')
    # df4_styler = resultt.style.set_table_attributes("style='display:inline'").set_caption('overall')

    # this is html code for the table?? sweet???
    # display_html(df1_styler._repr_html_() + df2_styler._repr_html_() +
    #              df3_styler._repr_html_() + df4_styler._repr_html_(), raw=True)
    # result = result[['imdb_title', 'imdb_year', 'imdb_director', 'imdb_rating']]
    # with pd.option_context('display.max_columns', None, 'display.max_rows', None):
    #     print(result)


def cleanCastMap():
    cast_matrix = pd.read_csv('unique_cast_matrix.csv', index_col=0)
    cast_matrix_temp = cast_matrix.sum(axis=1, skipna=True)
    cast_matrix_temp = cast_matrix_temp.mask(cast_matrix_temp == 0)
    cast_matrix = cast_matrix[cast_matrix_temp.notna()]
    cast_matrix.to_csv('unique_cast_matrix_clean.csv')


def cleanCastDatabase(con, imdb_movie):
    cast = pd.read_csv('unique_cast.csv')
    cast.columns = ['imdb_cast', 'count']
    cast = cast[cast['count'] >= 5]
    cast_list = cast['imdb_cast'].tolist()

    imdb_movie.imdb_cast = imdb_movie.imdb_cast.str.lower()

    imdb_movie['imdb_cast'] = imdb_movie['imdb_cast'].apply(
        lambda x: '  '.join([item for item in x.split(', ') if item in cast_list]))

    with pd.option_context('display.max_columns', None, 'display.max_rows', None):
        print(imdb_movie['imdb_cast'])

    imdb_movie.to_sql('imdb_cast', con=con, if_exists='replace', index=False)




def cleanDatabase(con, imdb_movie):
    cast = pd.read_csv('unique_cast.csv')
    cast.columns = ['imdb_cast', 'count']
    cast = cast[cast['count'] >= 5]
    cast_list = cast['imdb_cast'].tolist()

    imdb_movie.imdb_cast = imdb_movie.imdb_cast.str.lower()

    imdb_movie['imdb_cast'] = imdb_movie['imdb_cast'].apply(
        lambda x: '  '.join([item for item in x.split(', ') if item in cast_list]))

    key_words = pd.read_csv('unique_key.csv')
    key_words.columns = ['imdb_key', 'count']
    key_words = key_words[key_words['count'] >= 5]
    key_words = key_words['imdb_key'].tolist()

    imdb_movie['imdb_description'] = imdb_movie['imdb_description'].apply(
        lambda x: '  '.join([item for item in x.split(', ') if item in key_words]))

    imdb_movie.to_sql('imdb_movie_clean_new', con=con, if_exists='replace', index=False)


def main():
    print('hi1')
    # engine = create_engine(
    #     'mysql://masterUsername:masterPassword@imdbdataset.cylq1mw66qwl.ca-central-1.rds.amazonaws.com:3306/imdbdataset?charset=utf8')
    # con = engine.connect()
    rds_host = 'imdbdataset.cylq1mw66qwl.ca-central-1.rds.amazonaws.com'
    useruser = 'masterUsername'
    userpass = 'masterPassword'
    con = pymysql.connect(host=rds_host,user=useruser,password=userpass,port=3306,database='imdbdataset')
    print("hi2")
    imdb_movie = pd.read_sql_query('select * from imdb_movie_clean_new;', con)
    print("hi2")
    imdb_movie_map = pd.read_sql_query('select * from imdb_movie_map;', con, index_col=['index'])
    print("hi2")
    print(imdb_movie)

    input_title = 'Iron_Man'
    input_year = 2008

    input_title = input_title.replace('_', ' ')
    print(input_title)

    # cleanCastDatabase(con, imdb_movie)
    # cleanKeyDatabase(con, imdb_movie)
    # cleanDatabase(con, imdb_movie)
    # transferDatabase()

    # updateDatabase(imdb_movie, con)
    # countUniqueCast(imdb_movie)
    # countUniqueKeys(imdb_movie)

    # createGenreMap(imdb_movie)
    # createCastMap(imdb_movie)
    # createKeyMap(imdb_movie)
    # print('starting 5')
    # unique_cast = pd.read_csv('unique_cast_matrix_5.csv', index_col=0)
    # unique_cast_fitted = PCA(n_components=2).fit_transform(unique_cast)
    # unique_cast_fitted_df = pd.DataFrame(unique_cast_fitted, columns=['pca1', 'pca2'])



    # unique_key = pd.read_csv('unique_key_matrix_10.csv', index_col=0)
    # unique_cast = pd.read_csv('unique_cast_matrix_5_mod.csv', index_col=0)
    # unique_genre = pd.read_csv('unique_genre_matrix_mod.csv', index_col=0)
    #
    # unique_key = unique_key.multiply(0.618)

    # unique_cast = unique_cast.multiply(2.618)

    # unique_movie = unique_genre
    # unique_movie = unique_movie.join(unique_cast)
    # unique_movie = unique_movie.join(unique_key)
    #
    # unique_actors = unique_cast



    # unique_actors = unique_genre.join(unique_cast)
    # unique_movie = unique_movie.join(imdb_movie['imdb_rating'].divide(10))
    # print(unique_movie)


    # unique_movie_fitted = PCA(n_components=2).fit_transform(unique_movie)
    # imdb_movie_map = pd.DataFrame(unique_movie_fitted, columns=['pca1', 'pca2'])
    #
    # imdb_movie_map.to_sql(name='imdb_movie_map', con=con, if_exists='replace')



    #
    # unique_actors_fitted = PCA(n_components=2).fit_transform(unique_actors)
    # unique_actors_fitted_df = pd.DataFrame(unique_actors_fitted, columns=['pca1', 'pca2'])
    #
    # mpl.figure()
    # mpl.scatter(unique_actors_fitted_df.pca1, unique_actors_fitted_df.pca2, c='yellow')
    # unique_movie_fitted_df_1 = unique_movie_fitted_df[imdb_movie['imdb_id'].str.contains('tt1632708', regex=False)]
    # mpl.scatter(unique_movie_fitted_df_1.pca1, unique_movie_fitted_df_1.pca2, c='black')
    # unique_movie_fitted_df_1 = unique_movie_fitted_df[imdb_movie['imdb_id'].str.contains('tt0452594', regex=False)]
    # mpl.scatter(unique_movie_fitted_df_1.pca1, unique_movie_fitted_df_1.pca2, c='red')
    # unique_movie_fitted_df_1 = unique_movie_fitted_df[imdb_movie['imdb_id'].str.contains('tt2140619', regex=False)]
    # mpl.scatter(unique_movie_fitted_df_1.pca1, unique_movie_fitted_df_1.pca2, c='blue')
    # unique_movie_fitted_df_1 = unique_movie_fitted_df[imdb_movie['imdb_id'].str.contains('tt1142988', regex=False)]
    # mpl.scatter(unique_movie_fitted_df_1.pca1, unique_movie_fitted_df_1.pca2, c='cyan')
    # unique_movie_fitted_df_1 = unique_movie_fitted_df[imdb_movie['imdb_id'].str.contains('tt0111161', regex=False)]
    # mpl.scatter(unique_movie_fitted_df_1.pca1, unique_movie_fitted_df_1.pca2, c='orange')
    # mpl.show()

    # input_movie_options = imdb_movie[]
    # print(input_movie_options)

    input_coord = imdb_movie_map[(imdb_movie['imdb_title'].str.contains(input_title)
                                          & (imdb_movie['imdb_year'] == input_year))]
    print(imdb_movie_map)
    for column, contents in imdb_movie_map.items():
        imdb_movie_map[column] = imdb_movie_map[column].subtract(input_coord.iloc[:, imdb_movie_map.columns.
                                                     get_loc(column)].values[0], fill_value=0)
        imdb_movie_map[column] = imdb_movie_map[column].pow(2)
    imdb_movie_map['sum'] = imdb_movie_map.sum(axis=1, skipna=True)
    imdb_movie_map['dist'] = imdb_movie_map['sum'].pow(1. / 2)
    imdb_movie_map['rating'] = imdb_movie['imdb_rating'].multiply(-1)

    result = imdb_movie_map.nsmallest(25, ['dist'])
    result = imdb_movie.loc[result.nsmallest(10, ['rating']).index]
    result = result['imdb_title']

    # result = imdb_movie.loc[movie_map.nsmallest(20, ['dist']).index]

    with pd.option_context('display.max_columns', None, 'display.max_rows', None):
        # print(result)
        print(result)

    print(result.to_json(orient='values'))
    print(json.dumps(result.to_json(orient='values')))
    print(input_coord.to_json(orient='values'))

    # cast_matrix = pd.read_csv('unique_cast_matrix.csv', index_col=0)
    # key_matrix = pd.read_csv('unique_key_matrix.csv', index_col=0)
    #
    # priority_cast = [1] * imdb_movie.count(axis=1) / 12
    # priority_key = [1] * imdb_movie.count(axis=1) / 12
    #
    # start_time = time.time()
    #
    # input_cast = cast_matrix[imdb_movie['imdb_title'] == input_title]
    # input_key = key_matrix[imdb_movie['imdb_title'] == input_title]
    #
    # for item in input_cast[input_cast.ne(0)].dropna(axis=1):
    #     for row in cast_matrix[cast_matrix[item] > 0].index:
    #         priority_cast[row] = 0
    #
    # for item in input_key[input_key.ne(0)].dropna(axis=1):
    #     for row in key_matrix[key_matrix[item] > 0].index:
    #         priority_key[row] = 0
    #
    # cast_matrix['priority'] = priority_cast
    # key_matrix['priority'] = priority_key
    #
    # input_cast = cast_matrix[imdb_movie['imdb_title'] == input_title]
    # input_key = key_matrix[imdb_movie['imdb_title'] == input_title]
    # cast_matrix = cast_matrix.sort_values(by='priority', ascending=True)
    # for row in cast_matrix.head(100).itertuples():
    #     print(cast_matrix[row])
    #     print(row)

    # for column, contents in cast_matrix.items():
    #     cast_matrix[column] = cast_matrix[column].subtract(input_cast.iloc[:, cast_matrix.columns.
    #                                                  get_loc(column)].values[0], fill_value=0)
    #     cast_matrix[column] = cast_matrix[column].pow(2)
    # cast_matrix['sum'] = cast_matrix.sum(axis=1, skipna=True)
    # cast_matrix['dist'] = cast_matrix['sum'].pow(1. / 2)
    # cast_matrix['dist'] = cast_matrix['dist'].divide(10)
    #
    # for column, contents in key_matrix.items():
    #     key_matrix[column] = key_matrix[column].subtract(input_key.iloc[:, key_matrix.columns.
    #                                                  get_loc(column)].values[0], fill_value=0)
    #     key_matrix[column] = key_matrix[column].pow(2)
    # key_matrix['sum'] = key_matrix.sum(axis=1, skipna=True)
    # key_matrix['dist'] = key_matrix['sum'].pow(1. / 2)
    # key_matrix['dist'] = key_matrix['dist'].divide(5)
    #
    # print("--- %s seconds ---" % (time.time() - start_time))
    # cast_matrix['priority'] = priority_cast

    # result = imdb_movie.loc[cast_matrix.nsmallest(100, ['priority', 'dist']).index]
    #
    # with pd.option_context('display.max_columns', None, 'display.max_rows', None):
    #     print(result)

    # genreMap = pd.read_csv('unique_genre_map.csv', index_col=0)

    # castMap = cast_matrix['dist'].divide(10)
    # castMap['pca2'] = cast_matrix['priority'].divide(10)

    # print(castMap)
    # print(cast_matrix)

    # generateRecommendationProfiles(imdb_movie, input_title, genreMap, cast_matrix, key_matrix)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
